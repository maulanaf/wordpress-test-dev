<!-- START FOOTER -->
	<div id="footer">
		<div class="btop-1px"></div>
		<div class="container">
			<div class="four columns copyright">
				<img src="<?php echo get_template_directory_uri().'/images/logo-footer.png'; ?>" alt="" />
				<p class="last">@ 2012 All Rights Reserved.<br>Designed by <a href="http://themeforest.net/user/ZERGE">ZERGE</a> for <a href="http://themeforest.net">Themeforest</a>.</p>
			</div><!-- .copyright -->
			<div class="twelve columns">
				<div id="secondary-nav" class="clearfix">
					<ul class="clearfix">
						<li><a href="#">Home</a></li>
						<li><a href="#">Portfolio</a></li>
						<li><a href="#">Blog</a></li>
						<li><a href="#">Contact Us</a></li>
					</ul>
					<div class="gray-dash-3px"></div>
				</div><!-- #secondary-nav -->
				<ul id="footer-social">
	    			<li class="twitter"><a href="http://twitter.com"></a></li>
	    			<li class="dribbble"><a href="http://dribbble.com"></a></li>
	    			<li class="facebook"><a href="http://facebook.com"></a></li>
	    			<li class="google"><a href="http://plus.google.com"></a></li>
	    			<li class="linkedin"><a href="http://linkedin.com"></a></li>
	    			<li class="behance"><a href="http://behance.net"></a></li>
	    			<li class="rss"><a href="#"></a></li>
	    		</ul><!-- footer-social -->
			</div><!-- twelve columns -->
		</div><!-- container -->
	</div><!-- #footer -->
	<!-- END FOOTER -->

<div class="resize" title="Try resizing the window!"></div>


	
<!-- End Document
================================================== -->
</body>
</html>
<?php wp_footer(); ?>