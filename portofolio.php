<?php
/**
 * Template Name: portofolio
 *
 * Print posts of a Custom Post Type.
 */

get_header(); ?>
 <div class="container main-wrapper">
		<div id="main-content">


			<div id="filterby"></div>
	
			<!-- start filter -->
			<ul id="filters" class="clearfix">
				<li><a href="#" data-filter="*" class="selected">All</a></li>
				<li><a href="#" data-filter=".photography">Photography</a></li>
				<li><a href="#" data-filter=".illustration">Illustration</a></li>
				<li><a href="#" data-filter=".black-white">Black & white</a></li>
				<li><a href="#" data-filter=".video">Video</a></li>
			</ul>
			<!-- end filter -->
		<div id="container-portfolio" class="portfolio-4">
        <?php 
        $type = 'portofolio';
        $args = array (
         'post_type' => $type,
         'post_status' => 'publish',
         'paged' => $paged,
         'posts_per_page' => 8,
         'ignore_sticky_posts'=> 1
        );
        $temp = $wp_query; // assign ordinal query to temp variable for later use  
        $wp_query = null;
        $wp_query = new WP_Query($args); 
        if ( $wp_query->have_posts() ) :
            while ( $wp_query->have_posts() ) : $wp_query->the_post();
              ?>

		      <div class="item-block four columns portofolio">
					<!-- start item block -->
						<div class="stack">
							<div class="title"><a href="<?php the_permalink(); ?>"><?php the_title() ?> </a></div>
							<div class="cta-arrow">
								<a class="bg-arrow" href="<?php the_permalink(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/pixel-arrow-right.png" alt=""></a>
								<span class="rd-arrow"></span>
							</div><!-- .cta-arrow -->
						</div><!-- stack -->
						<div class="block">
						<?php if(the_category()=="video");
// echo hybrid_media_grabber( array( 'type' => 'video', 'split_media' => true) ); 	
						else;
							// the_content(); 
						endif; ?>
							<div class="mask"><a href="<?php the_permalink(); ?>" data-rel="zoom-img" class="zoom-icon"></a><a href="<?php the_permalink(); ?>" class="link-icon"></a></div>
						</div><!-- block -->  
				</div><!-- item-block -->
              <?php
            endwhile;
        else :
            echo '<h2>Not Found</h2>';
            get_search_form();
        endif;
        $wp_query = $temp;
        ?>
        		</div><!-- #container-portfolio -->
   <!-- START PAGINATION-->
			<div id="nav-pagination" class="sixteen columns">
				<ul class="nav-pagination clearfix">
				<?php
	   	    $args = array('total' => 4,
					'before_page_number' => '<li>',
					'after_page_number'  => '</li>',
					'prev_text'          => __('<li class="first"> « </li>'),
					'next_text'          => __('<li class="last"> »</li>'));
					echo paginate_links( $args ); ?>
	   	    	 </ul>
			</div><!-- #nav-pagination -->

		</div><!-- main-content -->

	</div><!-- .container -->
    <!-- START PORTFOLIO WRAPPER -->
	

			
	

			
<?php get_footer(); ?>