<?php
/**
 * The template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<?php if(have_posts()): ?>
			<?php while(have_posts()): the_post(); ?>
			
		<?php  get_template_part('content', get_post_format()); ?>

					
			

			<?php endwhile; else: ?>
					<p><?php echo ( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php endif; ?>

		

		
	<!-- END BLOG WRAPPER -->




<?php get_footer(); ?>