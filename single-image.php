<?php
/**
 * The template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<!-- START SEPARATOR  -->
	post image


			
				


			<!-- COMMENTS FORM -->
			<div id="comments-form">
			<div class="comment-block">
				<div class="comments-title">
						<h6><?php $post_id = get_the_ID();
																							$comments_count = wp_count_comments($post_id);
																							echo $comments_count->total_comments;
							   ?> Comments</h6>
						<div class="gray-dash-3px"></div>
					</div>
			</div>	
			<?php comments_template(); ?>
			
			</div><!-- #comments-form -->
			<!-- END COMMENTS FORM -->
		</div><!-- main-content -->
		<?php get_sidebar('single'); ?>

	
		
	</div><!-- .container -->

		
	<!-- END BLOG WRAPPER -->




<?php get_footer(); ?>