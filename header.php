<!DOCTYPE html>
<!-- A Color Theme design by ZERGE (http://www.color-theme.com) -->
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html <?php language_attributes(); ?>> <!--<![endif]-->

<head>

<meta charset="<?php bloginfo( 'charset' ); ?>">
 <title><?php wp_title(); ?></title>
 
	<?php wp_head(); ?>	


</head>
<body <?php body_class(); ?> >



<!-- Primary Page Layout -->

	<!-- START TOP PANEL -->
	<div id="top-panel">
		<div class="container panel">
			<div class="four columns location-map">
				<h6>About</h6>
				<div class="gray-dash-3px"></div>
				<p>Photomin is an American multinational compane that designs and markets consumer electronics, computer software, and personal computers.</p>
			</div><!-- .four -->		
			<div class="four columns">
				<h6>Contact Info</h6>
				<div class="gray-dash-3px"></div>
				<ul id="contact-info" class="widget clearfix">
					<li class="location">12345 / Some Street<br>Portland, USA</li>
					<li class="phone">phone: +1 (44)  123-45-67<br/>fax: +1 (44)  123-45-63</li>
					<li class="skype"><a href="skype:echo123?call">Skype Me</a></li>
					<li class="email"><a href="mailto:support@color-theme.com">support@color-theme.com</a></li>
				</ul>
			</div><!-- .four -->
			<div class="eight columns">
				<h6>Get In Touch</h6>
				<div class="gray-dash-3px"></div>
	        <fieldset class="info_fieldset_top">
	            <div id="note-top"></div>
	            <div id="contacts-form">
		            <form id="ajax-contact-form" action="javascript:alert('Was send!');">
		            	<input type="text" name="name" required placeholder="Your Name" class="required" />
		                <input type="email" name="email" required  placeholder="Your Email" class="last-item required" />
		             	<input type="text" name="url" placeholder="http://" style="display:none;" />
		                <input type="text" name="subject" placeholder="Subject" class="last-item required" style="display:none;" />	                
		                <div class="clear"></div>
		                <textarea name="message" id="message" required placeholder="Type your questions here..."></textarea>
		                <input type="submit" class="submit no-margin-bottom"  value="Send Message"/>
		                <span></span>
		            </form>
					<div class="clear"></div>
				</div> <!-- end #contact-form -->
			</fieldset>
			</div><!-- .eight -->
		</div><!-- .container -->
		<div class="toggle colored-bg">
			<a href="#" title="Pandora's Box!"></a>
		</div><!-- .toggle -->
	</div><!-- #top-panel -->
	<!-- END TOP PANEL -->


	<!-- START MENU WRAPPER -->
	<div id="menu-wrapper">
		<div class="container">
			<div class="four columns">
				<div id="logo">
					<a href="./"><img src=" <?php echo get_template_directory_uri().'/images/logo.png'; ?>" alt="Logo" /></a>
				</div><!-- .logo -->
			</div><!-- four columns -->
			<div class="twelve columns navigation">

			<?php $data = array(			'container_class' => 'fix-fish-menu',
											'container'	=> 'div',
											'container_id' => 'menu',
											'menu_class'      => 'sf-menu',
											'menu_id'         => 'nav',
											'theme_location' => 'primary' );

			 wp_nav_menu($data); ?>

				
			</div><!-- .twelve columns -->
		</div><!-- .container -->
	</div><!-- #slider-wraper -->
	<!-- END MENU WRAPPER -->

	