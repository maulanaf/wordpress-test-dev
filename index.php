 <?php get_header(); ?>
 <!-- START SEPARATOR  -->
	<div id="separator">
		<div class="btop-1px"></div>
		<div class="container">
			<!-- start separator -->
			<div class="sixteen columns">
				<h4 class="page-title">The Blog</h4>
			</div><!-- sixteen columns -->
		</div><!-- .container -->
		<div class="bbottom-1px"></div>
	</div><!-- #separator -->
	<!-- END SEPARATOR -->	



	<!-- START BLOG WRAPPER -->
	<div class="container main-wrapper">
		<div id="main-content" class="twelve columns">
		<?php if(have_posts()): ?>
			<?php while(have_posts()): the_post(); ?>
			
		<?php  get_template_part('content', get_post_format()); ?>
					
			

			<?php endwhile; else: ?>
					<p><?php echo ( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php endif; ?>


			
			<!-- START PAGINATION-->
			<div id="nav-pagination">
				<ul class="nav-pagination clearfix"> 

<?php 
$args = array('total' => 4,
	'before_page_number' => '<li>',
	'after_page_number'  => '</li>',
	'prev_text'          => __('<li class="first"> « </li>'),
	'next_text'          => __('<li class="last"> »</li>'));
echo paginate_links( $args ); ?>
	   	    	 </ul>
			</div><!-- #nav-pagination -->
		</div><!-- main-content -->
	
<?php get_sidebar('main'); ?>		
	</div><!-- .container -->

		
	<!-- END BLOG WRAPPER -->
<?php get_footer(); ?>