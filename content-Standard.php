<?php if(is_single()){?>
<!-- START SEPARATOR  -->
	<div id="separator">
		<div class="btop-1px"></div>
		<div class="container">
			<!-- start separator -->
			<div class="sixteen columns">
				<h4 class="page-title"><?php the_title() ?></h4>
			</div><!-- sixteen columns -->
		</div><!-- .container -->
		<div class="bbottom-1px"></div>
	</div><!-- #separator -->
	<!-- END SEPARATOR -->	
<!-- START BLOG WRAPPER -->
	<div class="container main-wrapper">
		<div id="main-content" class="twelve columns">


			

			<!-- POST #1 - IMAGE -->
			<!-- <div class="entry-post single-post format-image"> -->
			<div id="post-<?php the_ID(); ?>" <?php post_class("entry-post"); ?> >
				<div class="info-post">
					<div class="ribbon-wrapper">
						<div class="ribbon-front">
							<a href="blog-post.html" title="Image Post Type"><span class="blog-post-format"></span></a>
						</div>
						<div class="ribbon-edge-bottomleft"></div>
					</div><!-- ribbon-wrapper -->
			    </div><!-- info-post -->

				<div class="stack">
							<div class="meta-post">
						<div class="date" title="Posted Date"><span><?php  the_date();?></span></div>
						<div class="tags" title="Tags"><span><?php the_tags( 'Social tagging: ',' > ' ); ?></span></div>
						<div class="comments" title="Comments"><span><a href="#"><?php $post_id = get_the_ID();
																						$comments_count = wp_count_comments($post_id);
																						echo $comments_count->total_comments;
						   ?> </a></span></div>
						<div class="author" title="Author"><span><a href="#"><?php the_author(); ?></a></span></div>
						<div class="tweets" title="Tweet this"><span><a href="https://twitter.com/intent/tweet?text=Check%20out%20this%20photo&amp;url=http://html.color-theme.com/photomin/blog.html&amp;via=zesky">Tweet</a></span></div>						
					</div><!-- meta-post -->
				</div><!-- stack -->
				<div class="image-post">
					<!-- <img src="images/blog/img02.jpg" alt=""> -->
					<?php 
						if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
							the_post_thumbnail('homepage-thumb');
								} else { echo '<img src="'.get_template_directory_uri().'/images/blog/img02.jpg" alt="">';  }
					?>
					 </div><!-- post-image -->
				<div class="text-post clearfix">
						<div class="title-post">
						<h6><?php the_title(); ?></h6>	
						</div>
					<?php echo wp_trim_words( get_the_content(), 100 ) ?>
				</div><!-- text-post -->
				
				<div class="tag-meta-post">
					<div class="tags" title="Tags">
						<span><?php the_category( ', ' );  ?></span>
					</div><!-- tags -->
					<div class="share-post">
						<!-- AddThis Button BEGIN -->
						<!-- <div class="addthis_toolbox addthis_default_style ">
							<a href="http://www.addthis.com/bookmark.php?v=250&amp;pubid=ra-4d8f199d3537a1b7" class="addthis_button_compact">Share</a>
						</div>
						<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
						<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-4d8f199d3537a1b7"></script> -->
						<!-- AddThis Button END -->
					</div><!-- share-post -->
				</div><!-- tag-meta-post -->
			</div><!-- entry-post -->


			
				


			<!-- COMMENTS FORM -->
			<div id="comments-form">
			<div class="comment-block">
				<div class="comments-title">
						<h6><?php $post_id = get_the_ID();
								$comments_count = wp_count_comments($post_id);
								echo $comments_count->total_comments;
							   ?> Comments</h6>
						<div class="gray-dash-3px"></div>
					</div>
			</div>	
			<?php comments_template(); ?>
			
			</div><!-- #comments-form -->
			<!-- END COMMENTS FORM -->
</div><!-- main-content -->
		<?php get_sidebar('single'); ?>

	
		
	</div><!-- .container -->
<?php } else { ?>	

<div  <?php post_class("entry-post"); ?> >
				<div class="info-post">
					<div class="ribbon-wrapper">
						<div class="ribbon-front">
							<a href="blog-post.html" title="Image Post Type"><span class="blog-post-format" ></span></a>
						</div>
						<div class="ribbon-edge-bottomleft"></div>
					</div><!-- ribbon-wrapper -->
			    </div><!-- info-post -->

				<div class="stack">
					<div class="meta-post">
						<div class="date" title="Posted Date"><span><?php  the_date();?></span></div>
						<div class="tags" title="Tags"><span><?php the_tags( 'Social tagging: ',' > ' ); ?></span></div>
						<div class="comments" title="Comments"><span><a href="#"><?php $post_id = get_the_ID();
																						$comments_count = wp_count_comments($post_id);
																						echo $comments_count->total_comments;
						   ?> </a></span></div>
						<div class="author" title="Author"><span><a href="#"><?php the_author(); ?></a></span></div>
						<div class="tweets" title="Tweet this"><span><a href="https://twitter.com/intent/tweet?text=Check%20out%20this%20photo&amp;url=http://html.color-theme.com/photomin/blog.html&amp;via=zesky">Tweet</a></span></div>						
					</div><!-- meta-post -->
				</div><!-- stack -->
				<div class="image-post">
					<?php 
						if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
							the_post_thumbnail('homepage-thumb');
								} else { echo '<img src="'.get_template_directory_uri().'/images/blog/img02.jpg" alt="">';  }
					?>	
				</div><!-- post-image -->
			
				<div class="text-post clearfix">
						<div class="title-post">
						<h6><a href="<?php the_permalink(); ?>"> <?php the_title() ?> </a></h6>	
						</div>
				<?php echo wp_trim_words( get_the_content(), 100 ) ?>
				   	<a href="<?php the_permalink(); ?>" class="button read-more">Read More</a>					
				</div><!-- text-post -->
				<div class="divider-blog-1px"></div>
			</div><!-- entry-post -->

<?php } ?>
