<?php
if (! function_exists('wthemes_setup')) :
function wthemes_setup(){
	
	/**
	* custom template tags for this theme.
	*/	
	require( get_template_directory() . '/inc/template-tags.php');

	/**
	* custom functions that act independently of the theme tempaltes
	*/
	require( get_template_directory() . '/inc/tweaks.php');

	/**
	* Make theme available for translation
	* translations can be filed in the /languages/ directory
	* if you are building a theme based on wthemes user a find and replace
	* to chang 'wthemes' to the name of your theme in all the template files
	*/
	load_theme_textdomain( 'wthemes', get_template_directory().'/languages' );

	/**
	* add default post and comment RSS feed links to head
	*/

	//Automatic Feed Link
	add_theme_support( 'automatic-feed-links' );

	/**
	* Enable support for the Aside Post Format
	*/
	//post format
	add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio') );

	/**
	* thsi themes uses wp_nav_menu() in one location.
	*/
	//register nav menu
	register_nav_menus( array(
    'primary' => __( 'Navigasi Utama', 'wthemes' ),
  	) );

}
endif;
add_action( 'after_setup_theme','wthemes_setup');

//width content
if (! isset($content_width)){
	$content_width = 650;
}
 
 //comment-reply
if ( is_singular() ) wp_enqueue_script( "comment-reply" ); 



function Pertama_scripts(){
	
	wp_enqueue_style('style-base', get_template_directory_uri()."/css/base.css" ,'', '0.1', 'all' );
	wp_enqueue_style('style-skeleton', get_template_directory_uri()."/css/skeleton.css" ,'', '0.1', 'all' );
	wp_enqueue_style('style-style', get_template_directory_uri()."/css/style.css" ,'', '0.1', 'all' );
	wp_enqueue_style('style-camera', get_template_directory_uri()."/css/camera.css" ,'', '0.1', 'all' );
	wp_enqueue_style('style-mediaelementplayer.min', get_template_directory_uri()."/css/mediaelementplayer.min.css" ,'', '0.1', 'all' );
	wp_enqueue_style('style-sourtcuy', get_template_directory_uri()."/images/favicon.ico" ,'', '0.1', 'all' );
	wp_enqueue_style('style-apple2', get_template_directory_uri()."/images/apple-touch-icon-72x72.png" ,'', '0.1', 'all' );
	wp_enqueue_style('style-apple3', get_template_directory_uri()."/images/apple-touch-icon-114x114.png" ,'', '0.1', 'all' );
	
	wp_enqueue_script('script-easing', get_template_directory_uri()."/js/jquery.easing.1.3.js", array('jquery'), '0.1', false );
	wp_enqueue_script('script-superfish', get_template_directory_uri()."/js/jquery.easing.1.3.js", '', '0.1', false );
	wp_enqueue_script('script-camera', get_template_directory_uri()."/js/camera.min.js", '', '0.1', false );
	wp_enqueue_script('script-mobile', get_template_directory_uri()."/js/jquery.mobile.customized.min.js", '', '0.1', false );
	wp_enqueue_script('script-jcarusel', get_template_directory_uri()."/js/jcarousel.min.js", '', '0.1', false );
	wp_enqueue_script('script-mediaelement', get_template_directory_uri()."/js/mediaelement-and-player.min.js", '', '0.1', false );
	wp_enqueue_script('script-scrolltop', get_template_directory_uri()."/js/scrolltopcontrol.js", '', '0.1', false );
	wp_enqueue_script('script-contact', get_template_directory_uri()."/js/contact.js", '', '0.1', false );
	wp_enqueue_script('script-custom', get_template_directory_uri()."/js/custom.js", '', '0.1', false );
	// wp_enqueue_script('script-footer', footer_scripts(), '' , '0.1' ,true );

}
function footer_scripts(){
	?>
<script type="text/javascript">
/***************************************************
			Camera Slider
***************************************************/
jQuery.noConflict()(function($){
			$('#camera_wrap_1').camera({
				thumbnails: false,
				pagination: false,
				loader: 'bar',
				loaderPadding: 0,
				loaderStroke: 3,
				pagination: true,
				loaderColor: '#7d7d7d'
			});
		});
</script>

<script>
jQuery.noConflict()(function($){
		// create player
    $('#player2').mediaelementplayer({
        // add desired features in order
        // I've put the loop function second,
        features: ['playpause','loop','current','progress','duration']
    });
});
</script>
<?php 
}
add_action('wp_enqueue_scripts', 'footer_scripts', 999 );
add_action('wp_enqueue_scripts','Pertama_scripts');



//setting template list comment
function display_comment(){
	
?>
					<div class="comment-block">
						<div class="gravatar">
							<a href="#">
								 <?php echo get_avatar( get_the_author_meta( 'ID'),40); ?> 
							</a>
				        </div><!-- .gravatar -->
						<div class="comment-text clearfix">
							<span class="comment-info">
								<span class="italic" title="January 5, 2011 at 4:43 PM"><p><?php comment_time('H:i:s'); ?></p></span>
								<span class="italic"><?php comment_author(  ); ?> </span>
							</span>
							<p class="comment">
				        	<?php comment_text( ); ?> 	
				        	</p>
						</div><!-- end comment-text -->
					</div>

					 <div class="navigation">
  						<?php paginate_comments_links(); ?> 
					 </div>
<?php

}

function special_nav_class($classes, $item){
     if( in_array('current-menu-item', $classes) || in_array('current-menu-ancestor', $classes) ){
             $classes[] = 'active ';
     }
     return $classes;
}


add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

//image size
add_image_size( 'homepage-thumb', 674, 337, true ); // (cropped)


//register post thumbnails
add_theme_support( 'post-thumbnails' );

//widget

function theme_slug_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Main Sidebar', 'theme-slug' ),
        'id' => 'sidebar-1',
        'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
        'before_title' => '<h1>',
        'after_title' => '</h1>',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
    ) );
    register_sidebar( array(
        'name' => __( 'Page Sidebar', 'theme-slug' ),
        'id' => 'sidebar-2',
        'description' => __( 'Widgets in this area will be show on pages.', 'theme-slug' ),
        'before_title' => '<h1>',
        'after_title' => '</h1>',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
    ) );
    register_sidebar( array(
        'name' => __( 'Single Sidebar', 'theme-slug' ),
        'id' => 'sidebar-3',
        'description' => __( 'Widgets in this area will be show on single.', 'theme-slug' ),
        'before_title' => '<h1>',
        'after_title' => '</h1>',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
    ) );
}
//themse widget sidebar
add_action( 'widgets_init', 'theme_slug_widgets_init' );





//search form
function my_search_form( $form ) {
	$form = '<form role="search" method="get" id="searchform" class="searchform" action="' . home_url( '/' ) . '" >
	<div>
	<input type="text" value="' . get_search_query() . '" name="s" id="s" />
	
	</div>
	</form>';

	return $form;
}

add_filter( 'get_search_form', 'my_search_form' );

//add custom-header
add_theme_support( 'custom-header' );

//add custom-background
add_theme_support( 'custom-background' );

//add title tag
add_theme_support( 'title-tag' );

//include hyber media grabber
require_once('hybrid-media-grabber.php');

//suctom recent post widget
require_once("widget_custom.php");

//create post type
function create_post_type() {
  register_post_type( 'portofolio',
    array(
      'labels' => array(
        'name' => __( 'Portofolio' ),
        'singular_name' => __( 'Portofolio' )
      ),
      'public' => true,
      'taxonomies' => array('category'),   
      'has_archive' => true,
      'supports' => array('title','editor', 'thumbnail',  'page-attributes'),
      'show_ui' => true,
       'capability_type' => 'page',
       'hierarchical' => true,
       'menu_position' => 4,
       'rewrite' => true,
    )
  );
}

add_action( 'init', 'create_post_type' );