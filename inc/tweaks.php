<?php
/**
* Custom functions that act independently of the theme templates
* 
* Eventually, some of the functionality here could be replaced by core features
*
* @package wthemesP
* @since wthemesP 0.1
*/