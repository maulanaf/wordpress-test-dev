<?php
$commenter = wp_get_current_commenter();
$req = get_option( 'require_name_email' );
$aria_req = ( $req ? " aria-required='true'" : '' );
?>
<ol class="commentlist">
	<?php
		

		if ( have_comments()) :

		//Gather comments for a specific page/post 

		$post_id = get_the_ID();

		$comments = get_comments(array(
			'post_id' => $post_id,
			'status' => 'approve' //Change this to the type of comments to be displayed
		));

		//Display the list of comments
		wp_list_comments(array(
			'per_page' => 10, //Allow comment pagination
			'reverse_top_level' => false, //Show the latest comments at the top of the list
			'callback' => 'display_comment'), $comments);
	endif;
	?>
</ol>

<?php 
$comments_args = array(
        // change the title of send button 
        'label_submit'=>'Submit Comment',
        // change the title of the reply section
        'title_reply'=>'<div class="comments-title">
					<h6>Post Your Comment</h6>
					<div class="gray-dash-3px"></div>
				</div>',
        // remove "Text or HTML to be displayed after the set of comment fields"
        'comment_notes_after' => '', 
        'fields' => apply_filters( 'comment_form_default_fields', array(

		'author' => ( $req ? ' ' : '' ) .'<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' placeholder="Name" />',   

    	'email'  => ( $req ? ' ' : '' ) . '<input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' placeholder="Email"/>',

    	'url'    => '' ) ),
    	'comment_field' => '<p>' . '<textarea id="comment" name="comment" cols="45" rows="8" aria-required="true" placeholder="Post your comment here!" ></textarea>' .  '</p>',
);

comment_form($comments_args); ?>